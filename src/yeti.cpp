/*
Copyright 2011, 2012 Ray Hogenson

This file is part of YetiHack.

YetiHack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

YetiHack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with YetiHack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "yeti.h"
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;
void yeti::moveleft()
{
	--x;
}
void yeti::moveright()
{
	++x;
}
void yeti::moveup()
{
	--y;
}
void yeti::movedown()
{
	++y;
}
yeti::yeti(int x1, int y1, int sizex1, int sizey1)
{
	sizex = sizex1;
	sizey = sizey1;
	x = x1;
	y = y1;
}
void yeti::movetowards(int a, int b, vector<yeti> yetis, int i)
{
	int random = rand() % 2;
	int left;
	int right;
	int real;
	if (random == 0)
	{
		left = (x) + (sizex - a);
		right = a + (sizex - x);
		real = absolute(x - a);
		if (left < right && left < real)
		{
			bool empty = true;
			for (int j = 0; j < yetis.size(); ++j)
			{
				int anew = x - 1;
				if (anew > sizex)
				{
					anew = 0;
				}
				if (anew < 1)
				{
					anew = sizex;
				}
				if (i != j && anew == yetis[j].xpos() && yetis[j].ypos() == y)
				{
					empty = false;
				}
			}
			if (empty)
			{
				moveleft();
			}
		}
		else if (right < left && right < real)
		{
			bool empty = true;
			for (int j = 0; j < yetis.size(); ++j)
			{
				int anew = x + 1;
				if (anew > sizex)
				{
					anew = 0;
				}
				if (anew < 1)
				{
					anew = sizex;
				}
				if (i != j && anew == yetis[j].xpos() && yetis[j].ypos() == y)
				{
					empty = false;
				}
			}
			if (empty)
			{
				moveright();
			}
		}
		else
		{
			if (x - a < 0)
			{
				bool empty = true;
				for (int j = 0; j < yetis.size(); ++j)
				{
					int anew = x + 1;
					if (anew > sizex)
					{
						anew = 0;
					}
					if (anew < 1)
					{
						anew = sizex;
					}
					if (i != j && anew == yetis[j].xpos() && yetis[j].ypos() == y)
					{
						empty = false;
					}
				}
				if (empty)
				{
					moveright();
				}
			}
			else if (x - a > 0)
			{
				bool empty = true;
				for (int j = 0; j < yetis.size(); ++j)
				{
					int anew = x - 1;
					if (anew > sizex)
					{
						anew = 0;
					}
					if (anew < 1)
					{
						anew = sizex;
					}
					if (i != j && anew == yetis[j].xpos() && yetis[j].ypos() == y)
					{
						empty = false;
					}
				}
				if (empty)
				{
					moveleft();
				}
			}
		}
	}
	// now for b
	else
	{
		left = (y) + (sizey - b);
		right = b + (sizey - y);
		real = absolute(y - b);
		if (left < right && left < real)
		{
			bool empty = true;
			for (int j = 0; j < yetis.size(); ++j)
			{
				int bnew = y - 1;
				if (bnew > sizey)
				{
					bnew = 0;
				}
				if (bnew < 1)
				{
					bnew = sizey;
				}
				if (i != j && x == yetis[j].xpos() && yetis[j].ypos() == bnew)
				{
					empty = false;
				}
			}
			if (empty)
			{
				moveup();
			}
		}
		else if (right < left && right < real)
		{
			bool empty = true;
			for (int j = 0; j < yetis.size(); ++j)
			{
				int bnew = y + 1;
				if (bnew > sizey)
				{
					bnew = 0;
				}
				if (bnew < 1)
				{
					bnew = sizey;
				}
				if (i != j && x == yetis[j].xpos() && yetis[j].ypos() == bnew)
				{
					empty = false;
				}
			}
			if (empty)
			{
				movedown();
			}
		}
		else
		{
			if (y - b < 0)
			{
				bool empty = true;
				for (int j = 0; j < yetis.size(); ++j)
				{
					int bnew = y + 1;
					if (bnew > sizey)
					{
						bnew = 0;
					}
					if (bnew < 1)
					{
						bnew = sizey;
					}
					if (i != j && x == yetis[j].xpos() && yetis[j].ypos() == bnew)
					{
						empty = false;
					}
				}
				if (empty)
				{
					movedown();
				}
			}
			else if (y - b > 0)
			{
				bool empty = true;
				for (int j = 0; j < yetis.size(); ++j)
				{
					int bnew = y - 1;
					if (bnew > sizey)
					{
						bnew = 0;
					}
					if (bnew < 1)
					{
						bnew = sizey;
					}
					if (i != j && x == yetis[j].xpos() && yetis[j].ypos() == bnew)
					{
						empty = false;
					}
				}
				if (empty)
				{
					moveup();
				}
			}
		}
	}
	// deal with going off the side
	if (x < 1)
	{
		x = sizex;
	}
	if (x > sizex)
	{
		x = 1;
	}
	if (y < 1)
	{
		y = sizey;
	}
	if (y > sizey)
	{
		y = 1;
	}
}

int yeti::xpos()
{
	return x;
}
int yeti::ypos()
{
	return y;
}
int yeti::absolute(int value)
{
	if (value < 0)
	{
		value = 0 - value;
	}
	return value;
}

int yeti::setx(int newx)
{
	x = newx;
}

int yeti::sety(int newy)
{
	y = newy;
}

