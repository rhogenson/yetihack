#ifndef YETI_H
#define YETI_H

#include <vector>
using namespace std;
class yeti
{
	public:
		void moveleft();
		void moveright();
		void moveup();
		void movedown();
		yeti(int, int, int, int);
		void movetowards(int, int, vector<yeti>, int);
		int xpos();
		int ypos();
		int setx(int);
		int sety(int);
	private:
		int x;
		int y;
		int sizex;
		int sizey;
		int absolute(int);
};
#endif
